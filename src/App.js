import React from "react";
import List from "./views/List";

import { ColorContext, colors } from "./context/theme";

function App() {
  return (
    <ColorContext.Provider value={colors}>
      <div className="App">
        <main className="container">
          <div className="divider"></div>
          <List />
        </main>
      </div>
    </ColorContext.Provider>
  );
}

export default App;
