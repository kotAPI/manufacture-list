import manufacturerReducer from "./manufacturerReducer";
import { combineReducers } from "redux";

//Combine all the sub reducers
const rootReducer = combineReducers({
  manufacturers: manufacturerReducer,
});

export default rootReducer;
