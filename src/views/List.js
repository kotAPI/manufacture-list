import React, { useEffect, useMemo, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import Item from "../components/Item";
import { ColorContext } from "../context/theme";

const { getManufacturerList } = require("../api");

const TodoList = (props) => {
  //#2 Used to get a single attribute or object inside the Reducer
  console.log(props);
  const colors = useContext(ColorContext);
  //Get todoList from todoReducer
  const manufacturersList = useSelector(
    (state) => state.manufacturers.manufacturersList
  );

  const calculation = useMemo(() => expensiveCalculation(1), []);
  console.log("Memoization result", calculation);
  //Use for all the dispatch actions
  const dispatch = useDispatch();

  const getManufacturers = async () => {
    let res = await getManufacturerList();
    let list = res.data.Results;
    dispatch({ type: "UPDATE_LIST", payload: list });
  };

  //Handle onClick event
  const likeManufacturer = (item) => {
    dispatch({ type: "LIKE_MANUFACTURER", payload: item });
  };

  useEffect(() => {
    getManufacturers();
  }, []);

  return (
    <section id="section-todo">
      <h3 className="center-align" style={{ backgroundColor: colors.blue }}>
        Manufacturers List
      </h3>
      {manufacturersList.length > 0 ? (
        <ul className="collection">
          {manufacturersList.map((item, i) => {
            return (
              <Item likeManufacturer={likeManufacturer} key={i} item={item} />
            );
          })}
        </ul>
      ) : (
        <p className="center-align">Nothing to show here</p>
      )}
    </section>
  );
};

const expensiveCalculation = (num) => {
  console.log("Calculating...");
  for (let i = 0; i < 1000000000; i++) {
    num += 1;
  }
  return num;
};

export default TodoList;
