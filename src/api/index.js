import axios from "axios";

const url =
  "https://vpic.nhtsa.dot.gov/api/vehicles/getallmanufacturers?format=json";

export const getManufacturerList = () => {
  return axios.get(url);
};
