import React from "react";
import HeartBorder from "../asset/heart-border.svg";
import Heart from "../asset/heart.svg";

//Single todo item component
const Item = (props) => {
  return (
    <li className="collection-item" key={props.item.Mfr_ID}>
      {props.item.Mfr_Name || "N/A"}
      <div
        onClick={() => {
          props.likeManufacturer(props.item);
        }}
        className="secondary-content"
      >
        {props.item.liked ? (
          <img src={Heart} alt="" />
        ) : (
          <img src={HeartBorder} alt="" />
        )}
      </div>
      <div>
        <ul>
          {props.item.VehicleTypes.map((item, i) => {
            return (
              <li className="vehicle" key={i}>
                {item.Name}
              </li>
            );
          })}
        </ul>
      </div>
    </li>
  );
};

export default Item;
