const initState = {
  manufacturersList: [],
};

const todoReducer = (state = initState, action) => {
  switch (action.type) {
    case "LIKE_MANUFACTURER":
      let newList = state.manufacturersList.map((item) => {
        if (item.Mfr_ID === action.payload.Mfr_ID) {
          item.liked = !item.liked;
        }
        return item;
      });
      console.log(newList);
      //
      return {
        ...state,
        manufacturersList: newList,
      };
    case "UPDATE_LIST":
      return {
        ...state,
        manufacturersList: action.payload,
      };
    default:
      return state;
  }
};

export default todoReducer;
